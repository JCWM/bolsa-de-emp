class Article < ActiveRecord::Base
mount_uploader :picture, ArticleImageUploader
validates :title, presence: true
validates :sumario, presence: true
validates :content, presence: true
validates :data, presence: true
scope :containnoticia, -> (name) { where("title like ? or content like ? ", "%#{name}%","%#{name}%")}
scope :containbackoffice, -> (name) { where("title like ?", "%#{name}%")}
end

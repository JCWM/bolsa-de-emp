class Offer < ActiveRecord::Base
      belongs_to :user

     validates :user_id, presence: true
     validates :content, presence: true, length: { maximum: 600 }
     validates :title, presence: true, length:{maximum: 100}
     #validates_date :dataInicio, :on_or_after => lambda { Date.current }
     validates_date :dataFim, :on_or_after => lambda { Date.current }
     mount_uploader :picture, PictureUploader
     validate  :picture_size
     scope :containoferta, -> (name,tipocontrato,actividadeProf) { where("title like ? and tipocontrato like ? and  actividadeProf like ?", "%#{name}%","%#{tipocontrato}%","%#{actividadeProf}%")}


#candidaturas
 has_many :passive_candidatures, class_name: "Candidature",
                                foreign_key: "offer_id",
                                dependent: :destroy

  has_many :offerCandidates, through:  :passive_candidatures, source: :user

  # Follows a user.
  def candidatar(offer)
    active_candidatures.create(offer_id: offer.id)
  end

  # Unfollows a user.
  def descandidatar(offer)
    active_candidatures.find_by(offer_id: offer.id).destroy
  end

  # Returns true if the current user is following the other user.
  def userCandidates?(offer)

    userCandidates.include?(offer)
  end
  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end


end

class Candidature < ActiveRecord::Base
  belongs_to :user, class_name: "User"
  belongs_to :offer, class_name: "Offer"
  validates :user_id, presence: true
  validates :offer_id, presence: true
end

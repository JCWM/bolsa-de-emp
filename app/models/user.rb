class User < ActiveRecord::Base
  #assegurar que quando um utilizador for destruido tambem é destruida a oferta

  #novo modelo ofertas
  has_many :offers, dependent: :destroy
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy

  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

#candidatarme
  has_many :active_candidatures, class_name: "Candidature",
                                 foreign_key: "user_id",
                                 dependent: :destroy

  has_many :userCandidates, through: :active_candidatures, source: :offer





  attr_accessor :remember_token, :activation_token, :reset_token
  #metodos antecedentes às açoes
  before_save   :downcase_email
  before_create :create_activation_digest
  #uploaders
  mount_uploader :picture, PictureUploader
  mount_uploader :cv, AttachmentUploader
  #validaçoes sql
  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX, message: "Email Inválido"  },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true


  validate  :picture_size
  validate  :cv_size

  VALID_CODP_REGEX = /([1-9][0-9]{3}-[1-9][0-9]{2})|([1-9][0-9]{3}-[a-zA-Z]{3,6})/i #exemplos validos :4485-710 4485-VCD ou vazio
  validates :codPostal, format: { with: VALID_CODP_REGEX, message: "inválido (ex: 4485-710 ou 4485-VCD ou vazio)" ,allow_nil: true ,allow_blank: true}
  validates :morada, length:{maximum:255,message: "ultrapassou os caracteres permitidos. (255 car.)"}
  validates :apresentacao, length:{maximum:356,message: "ultrapassou os caracteres permitidos. (356 car.)"}
  validates :habilitlit, length:{maximum:356,message: "ultrapassou os caracteres permitidos. (356 car.)"}
  validates :expProf, length:{maximum:356,message: "ultrapassou os caracteres permitidos. (356 car.)"}

=begin nao sao necessarios, sao escolhidos com droplist
  VALID_APROF_REGEX = /(Progamador Web|Eng de software|Eng de sistemas|outro)\z/i
  validates :areaprof, format: { with: VALID_APROF_REGEX , message: "Área profissional Inválida"}

  VALID_SITPROF_REGEX = /(Empregado|Desempregado)\z/i
  validates :situacaoprof, format: { with: VALID_SITPROF_REGEX , message: "Situação profissional Inválida"}

  VALID_ATIVIDADEPROF_REGEX = /(Progamador Web|Eng de software|Eng de sistemas|outro)\z/i
  validates :atividadeprofissional, format: { with: VALID_ATIVIDADEPROF_REGEX , message: "Área profissional Inválida"}
=end

  VALID_TEL_REGEX = /((91)|(92)|(93)|(94)|(95)|(96))[0-9]{7}/i
  validates :telemovel, format:{with: VALID_TEL_REGEX, message: " inválido" ,allow_nil: true ,allow_blank: true}

  VALID_BI_REGEX = /[0-9]{8}\z/i
  validates :bi, format:{with: VALID_BI_REGEX, message: " inválido" ,allow_nil: true ,allow_blank: true}

  VALID_NIF_REGEX = /[0-9]{9}\z/i
  validates :nif, format:{with: VALID_BI_REGEX, message: " inválido" ,allow_nil: true ,allow_blank: true}



  validates :paginaweb, format: { with: URI.regexp ,allow_nil: true ,allow_blank: true}
  validates :paginapessoal, format: { with: URI.regexp ,allow_nil: true ,allow_blank: true}


  #scopes para filtragem (search)

  scope :containent, -> (name,actividadeprofissional,localidade) { where("name like ? and tipouser='entidade' and actividadeprofissional like ? and localidade like ?", "%#{name}%","%#{actividadeprofissional}%","%#{localidade}%")}
  scope :containcand, -> (name,areaprof,situacaoprof,localidade) { where("name like ? and tipouser='candidato' and areaprof like ? and situacaoprof like ? and localidade like ?", "%#{name}%","%#{areaprof}%","%#{situacaoprof}%","%#{localidade}%")}
scope :contain, -> (name,tipo) { where("name like ?  and tipouser like ? ", "%#{name}%","%#{tipo}%")}



  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end


 # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end



  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Oferta.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)

  end




  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end



  # Follows a user.
  def candidatar(offer)
    active_candidatures.create(offer_id: offer.id)
  end

  # Unfollows a user.
  def descandidatar(offer)
    active_candidatures.find_by(offer_id: offer.id).destroy
  end

  # Returns true if the current user is following the other user.
  def userCandidates?(offer)

    userCandidates.include?(offer)
  end


##Private Methods bellow
private
    def downcase_email
      self.email = email.downcase
    end

    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
    # Validates the size of an uploaded picture.

    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

    # Validates the size of an uploaded picture.
    def cv_size
      if cv.size > 5.megabytes
        errors.add(:cv, "should be less than 5MB")
      end
    end


end

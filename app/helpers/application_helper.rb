module ApplicationHelper


  def full_title(page_title = '')                     # Method def, optional arg
    base_title = "BolsaDeEmpApp"  # Variable assignment
    if page_title.empty?                              # Boolean test
      base_title                                      # Implicit return
    else
      page_title + " | " + base_title                 # String concatenation
    end
  end


    def correct_user
      @user = User.find(params[:id])
      if(current_user.tipouser != "admin")
      redirect_to(root_url) unless @user == current_user
      end
    end





# Confirms an admin user.
    def admin_user
      if(current_user.tipouser != "admin")
          redirect_to(root_url)
      end
    end

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Por favor faça log in."
        redirect_to login_url
      end
    end

end

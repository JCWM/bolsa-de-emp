class OffersController < ApplicationController
    before_action :logged_in_user, only: [:new, :create, :edit, :update ]
    before_action :correct_user,   only: [:edit, :update]
    before_action :admin_user,     only: [:destroy]
  include ApplicationHelper
  def index

    @offers = Offer.containoferta(params[:name],params[:tipocont],params[:area]).paginate(page: params[:page], :per_page => 8)
  end

def create
    @oferta = current_user.offers.build(oferta_params)
    if @oferta.save
      flash[:success] = "Oferta criada!"
      redirect_to @oferta
   else
    render 'new'
    end
  end

def new
  @offer=Offer.new
end


  def destroy
    Offer.find(params[:id]).destroy
    flash[:success] = "Oferta removida"
    redirect_back_or(root_url)
  end



def edit
 @offer = Offer.find(params[:id])
end

def showOfertas

    @areas=Offer.uniq.pluck(:actividadeProf).reject{ |f| f.nil? || f.empty? }
    @tiposcont=Offer.uniq.pluck(:tipocontrato).reject{ |f| f.nil? || f.empty? }
    @offers = Offer.all.where(ativado: true).order(updated_at: :desc, created_at: :desc).containoferta(params[:name],params[:tipocontrato],params[:actividadeProf]).paginate(page: params[:page], :per_page => 8)
end

def show

 @offer = Offer.find(params[:id])
 @owner = User.find(@offer.user_id)
 @ofertas = Offer.where("actividadeProf = ? and ativado = ?", @offer.actividadeProf, true).paginate(page: params[:page],:per_page => 2)
 @candidatos = @offer.offerCandidates.paginate(page: params[:page],:per_page => 2)
end



def update
    @offer = Offer.find(params[:id])
    if @offer.update_attributes(oferta_params)
      flash[:success] = "Oferta atualizada"
      redirect_to @offer
    else
     render 'edit'
    end
end

private

def oferta_params
        params.require(:offer).permit(:content, :picture, :title, :dataInicio, :dataFim, :actividadeProf,:tipocontrato,:salario,:ativado)
end


end

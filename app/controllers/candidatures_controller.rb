class CandidaturesController < ApplicationController
    include ApplicationHelper
  def create
    @offer = Offer.find(params[:offer])
    current_user.candidatar(@offer)
    respond_to do |format|
      format.html { redirect_to @offer }
      format.js
    end
  end

  def destroy
    @offer = Candidature.find(params[:id]).offer
    current_user.descandidatar(@offer)
    respond_to do |format|
      format.html { redirect_to @offer }
      format.js
    end
  end

end

class BackofficeController < ApplicationController

  before_action :admin_user
  include ApplicationHelper
def home
end
#show all editable users
def editUsers
  store_location
  @users=User.all
  @tipos=User.uniq.pluck(:tipouser).reject{ |f| f.nil? || f.empty? }
 @users = User.contain(params[:name],params[:tipo])
end

#edit a single user
def editUser
store_location
@user = User.find(params[:id])
end

def editPw
@user = User.find(params[:id])
end


def inserirUser
@user = User.new
end
def inserirNoticia
@noticia = Article.new
end

def editNoticias
store_location
@noticias = Article.order(destaque: :desc,data: :desc).containbackoffice(params[:name])
end

def editNoticia
store_location
@noticia=Article.find(params[:id])
end

def show

end

def detalhesUser
 @user = User.find(params[:id])
end

def detalhesNoticia
 @noticia = Article.find(params[:id])
end
private

# Confirms an admin user.

end

class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :following, :followers]
  before_action :correct_user,   only: [:edit, :update ]
  before_action :admin_user,     only: :destroy

  include ApplicationHelper

  def self.search(search)
    if search
      @users=User.where('name LIKE ?', "%#{search}%")
    else

    end
  end


def index
@users = User.where.not(tipouser: "admin").paginate(page: params[:page],:per_page => 8)
end


def perfilent
@offersAtivas=current_user.offers.where(ativado: true).order(updated_at: :desc, created_at: :desc).paginate(page: params[:o_page],:per_page => 2)
@historicoOffers=current_user.offers.where(ativado: false).order(updated_at: :desc, created_at: :desc).paginate(page: params[:ho_page],:per_page => 2)
@candinteressados=current_user.followers.paginate(page: params[:c_page],:per_page => 2)
end
def perfilcand
@empinteressadas=current_user.followers.paginate(page: params[:e_page],:per_page => 2)
@candidaturas=current_user.userCandidates.paginate(page: params[:c_page],:per_page => 2)
end


def show
 @user = User.find(params[:id])
end

def showCandidatos
  @areasprof =User.uniq.pluck(:areaprof).reject{ |f| f.nil? || f.empty? }
  @localidades =User.where(tipouser: "candidato").uniq.pluck(:localidade).reject{ |f| f.nil? || f.empty? }
  @situacoesprof=User.where(tipouser: "candidato").uniq.pluck(:situacaoprof).reject{ |f| f.nil? || f.empty? }
  @users = User.where(tipouser: "candidato").containcand(params[:name],params[:areaprof],params[:situacaoprof],params[:localidade]).paginate(page: params[:page], :per_page => 8)
end

def showEntidades
  @atividadesprof =User.uniq.pluck(:actividadeprofissional).reject{ |f| f.nil? || f.empty? }
  @localidades =User.where(tipouser: "entidade").uniq.pluck(:localidade).reject{ |f| f.nil? || f.empty? }
  @users = User.containent(params[:name],params[:actividadeprofissional],params[:localidade]).paginate(page: params[:page], :per_page => 8)
end

def new
 @user = User.new
end


#cria um candidato valo tipouser = candidato
def newCandidato
@user= User.new
end

def newEntidade
@user= User.new
end

def create
@user = User.new(user_params)
    if @user.save
      if(@user.activated?)
      redirect_to @user
      else
      UserMailer.account_activation(@user).deliver_now
      flash[:info] = "Verifique o seu email para ativar a conta."
      redirect_to root_url
      end
    else
      if logged_in? && current_user.tipouser=='admin'
        render 'backoffice/inserirUser'
      else
        if @user.tipouser=='entidade'
        render 'newEntidade'
        else
        render 'newCandidato'
        end
      end
    end
end

def edit
store_location
end

def editPwUser
 @user = User.find(current_user.id)
end

 def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Perfil atualizado"
      redirect_to @user
    else
      render 'edit'
    end
end

 def updatePW
    @user = User.find(current_user.id)
    if @user.authenticate(params[:current_password]) && @user.update_attributes(user_params)
      flash[:success] = "Password atualizada"
      redirect_to @user
    else
      flash.now[:danger] = "Password atual incorreta" unless @user.authenticate(params[:current_password])
      render 'editPwUser'
    end
end


  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Utilizador removido"
    redirect_back_or(root_url)
  end

  def following
    @title = "A Seguir"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Seguidores"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end




    private

    def user_params
      params.require(:user).permit(:name,
                                    :email,
                                    :password,
                                   :password_confirmation,
                                   :tipouser,
                                   :picture,
                                   :cv,
                                   :morada,
                                   :localidade,
                                   :codPostal,
                                   :telemovel,
                                   :datadenasc,
                                   :paginapessoal,
                                   :bi,
                                   :areaprof,
                                   :apresentacao,
                                   :niveldehab,
                                   :habilitlit,
                                   :situacaoprof,
                                   :expProf,
                                   :fax,
                                   :nif,
                                   :actividadeprofissional,
                                   :paginaweb,
                                   :activated)
    end

# Before filters



end

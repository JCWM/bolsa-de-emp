class ArticlesController < ApplicationController
    include ApplicationHelper
  def index
@articles = Article.containnoticia(params[:name]).paginate(page: params[:page], :per_page => 8)
  end
 def new
 end

 def create
  @noticia = Article.new(article_params)
    if @noticia.save
      redirect_to @noticia
    else
      render 'backoffice/inserirNoticia'
    end
 end


 def show
  @article=Article.find(params[:id])
 end
def showNoticias
  @articles = Article.where(activated: true).order(destaque: :desc,data: :desc).containnoticia(params[:name]).paginate(page: params[:page], :per_page => 8)

end
 def update
    @article = Article.find(params[:id])
    if @article.update_attributes(article_params)
      flash[:success] = "Notícia atualizada"
      redirect_to @article
    else
      render 'editNoticia'
    end
end


  def destroy
    Article.find(params[:id]).destroy
    flash[:success] = "Noticia apagada"
    redirect_back_or(root_url)
  end


 private

    def article_params
      params.require(:article).permit(:title,
                                   :content,
                                   :sumario,
                                   :destaque,
                                   :data,
                                   :picture,
                                   :activated)
    end

end

class SessionsController < ApplicationController
    include ApplicationHelper
  def new
  end

def choosetype
  render 'choose'
end
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
          if current_user.tipouser=='entidade'
          redirect_to perfilent_path
          else
          if current_user.tipouser=='candidato'
            redirect_to perfilcand_path
          else
            redirect_to backoffice_path
          end
          end
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end

BolsaDeEmpApp::Application.routes.draw do
root             'static_pages#home'

#backoffice
get 'backoffice'  => 'backoffice#home'
get 'editUsers' => 'backoffice#editUsers'
get 'editNoticias' => 'backoffice#editNoticias'
get 'backofficeDetalhesUser' => 'backoffice#detalhesUser'
get 'editUser' => 'backoffice#editUser'
get 'inserirUser' => 'backoffice#inserirUser'
get 'editPw' => 'backoffice#editPw'
get 'editNoticia' => 'backoffice#editNoticia'
get 'backofficeDetalhesNoticia' => 'backoffice#detalhesNoticia'
get 'inserirNoticia' => 'backoffice#inserirNoticia'



#users
resources :users do
  member do
    get :following, :followers
    get :userCandidates
  end
end
get 'showEntidades' => 'users#showEntidades'
get 'showCandidatos' => 'users#showCandidatos'
get 'editPwUser' => 'users#editPwUser'
post 'updatePW' => 'users#updatePW'
get 'perfilent' => 'users#perfilent'
get 'perfilcand' => 'users#perfilcand'
post 'candidatosignup'  => 'users#newCandidato'
post 'entidadesignup' => 'users#newEntidade'
get 'users/new'


#session
get 'sessions/new'
get 'signup'  => 'sessions#choose'
get    'login'   => 'sessions#new'
post   'login'   => 'sessions#create'
delete 'logout'  => 'sessions#destroy'

#offers
resources :offers
get 'offers/new'
get 'showOfertas' => 'offers#showOfertas'

#articles
resources :articles
get 'articles/new'
get 'showNoticias'  => 'articles#showNoticias'

#relationship
resources :relationships,       only: [:create, :destroy]

#Candidatures
resources :candidatures,       only: [:create, :destroy]
get 'candidatures/new'


#account activation
resources :account_activations, only: [:edit]

#password reset
resources :password_resets,     only: [:new, :create, :edit, :update]
  get 'password_resets/new'
  get 'password_resets/edit'

end

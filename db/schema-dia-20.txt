# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151120110309) do

  create_table "noticia", force: :cascade do |t|
    t.text     "title"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

# Could not dump table "oferta" because of following NoMethodError
#   undefined method `[]' for nil:NilClass


  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "tipouser",                              default: "candidato"
    t.string   "activation_digest"
    t.boolean  "activated",                             default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "morada"
    t.string   "localidade"
    t.string   "codPostal"
    t.string   "telemovel"
    t.datetime "datadenasc"
    t.string   "paginapessoal"
    t.string   "bi"
    t.string   "areaprof"
    t.string   "apresentacao"
    t.string   "niveldehab"
    t.string   "habilitlit"
    t.string   "situacaoprof"
    t.string   "cv"
    t.string   "picture"
    t.string   "expProf"
    t.string   "fax"
    t.string   "nif"
    t.string   "actividadeprofissional"
    t.string   "paginaweb"
    t.integer  "{:index=>true, :foreign_key=>true}_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true


     create_table "oferta", force: :cascade do |t|
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.text "title"
    t.text "content"
    t.timestamps null: false
    t.references :user, index: true, foreign_key: true
    t.date  "dataInicio"
    t.date  "dataFim"
    t.text  "actividadeProf"
    t.text  "tipocontrato"
    t.text  "salario"
    t.boolean "ativado", default: false
    t.string  "picture"
  end

       add_index :oferta, [:user_id, :created_at]
end

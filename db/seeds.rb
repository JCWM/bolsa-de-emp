# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

##Admin
User.create!(name:  "Joao",
             email: "joao@joao.pt",
             password:              "123123",
             password_confirmation: "123123",
             tipouser: "admin",
             activated: true,
             activated_at: Time.zone.now)

##Candidatos com foto e cv 
15.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  morada = Faker::Address.street_address
  localidade= Faker::Address.city
  codPostal="4485-888"
  telemovel="919999999"
  datanasce=Faker::Date.backward(14)
  paginapessoal=Faker::Internet.url
  bi=Faker::Number.number(8)
  areaprof="Informatica"
  apresentacao=Faker::Lorem.sentence
  niveldehab="Secundário"
  habilitlit=Faker::Lorem.sentence
  situacaoprof="Desempregado"
  expProf=Faker::Lorem.sentence
  user=User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
                  tipouser: "candidato",
                  morada: morada,
                  localidade: localidade,
                  codPostal: codPostal,
                  telemovel: telemovel,
                  datadenasc: datanasce,
                  paginapessoal: paginapessoal,
                  bi: bi,
                  areaprof: areaprof,
                  apresentacao: apresentacao,
                  niveldehab: niveldehab,
                  habilitlit: habilitlit,
                  situacaoprof: situacaoprof,
                  expProf: expProf,
              activated: true,
              activated_at: Time.zone.now)
  user.picture=File.open(Rails.root.to_s + "/db/images/c.png")
  user.cv=File.open(Rails.root.to_s + "/db/images/cv.pdf")
  user.save!

end
##Candidatos so com cv 
15.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+16}@railstutorial.org"
  password = "password"
  morada = Faker::Address.street_address
  localidade= Faker::Address.city
  codPostal="4485-888"
  telemovel="919999999"
  datanasce=Faker::Date.backward(14)
  paginapessoal=Faker::Internet.url
  bi=Faker::Number.number(8)
  areaprof="Informatica"
  apresentacao=Faker::Lorem.sentence
  niveldehab="Secundário"
  habilitlit=Faker::Lorem.sentence
  situacaoprof="Desempregado"
  expProf=Faker::Lorem.sentence
  user=User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
                  tipouser: "candidato",
                  morada: morada,
                  localidade: localidade,
                  codPostal: codPostal,
                  telemovel: telemovel,
                  datadenasc: datanasce,
                  paginapessoal: paginapessoal,
                  bi: bi,
                  areaprof: areaprof,
                  apresentacao: apresentacao,
                  niveldehab: niveldehab,
                  habilitlit: habilitlit,
                  situacaoprof: situacaoprof,
                  expProf: expProf,
              activated: true,
              activated_at: Time.zone.now)

end




#entidades com foto 

15.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+31}@railstutorial.org"
  password = "password"
  morada = Faker::Address.street_address
  localidade= Faker::Address.city
  codPostal="4485-999"
  telemovel="919999999"
  datanasce=Faker::Date.backward(14)
  paginapessoal=Faker::Internet.url
  nif=Faker::Number.number(9)
 atividadeprofissional='Progamador Web'
  apresentacao=Faker::Lorem.sentence
  user=User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
                  tipouser: "entidade",
                  morada: morada,
                  localidade: localidade,
                  codPostal: codPostal,
                  telemovel: telemovel,
                  fax: "44-208-1234567",
                  paginaweb: paginapessoal,
                  nif:nif,
                  apresentacao:apresentacao,
              activated: true,
              activated_at: Time.zone.now,
              actividadeprofissional: atividadeprofissional)
  user.picture=File.open(Rails.root.to_s + "/db/images/e.png")
  user.save!
end
#entidade sem foto
15.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+46}@railstutorial.org"
  password = "password"
  morada = Faker::Address.street_address
  localidade= Faker::Address.city
  codPostal="4485-999"
  telemovel="919999999"
  datanasce=Faker::Date.backward(14)
  paginapessoal=Faker::Internet.url
  nif=Faker::Number.number(9)
 atividadeprofissional='Progamador Web'
  apresentacao=Faker::Lorem.sentence
  user=User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
                  tipouser: "entidade",
                  morada: morada,
                  localidade: localidade,
                  codPostal: codPostal,
                  telemovel: telemovel,
                  fax: "44-208-1234567",
                  paginaweb: paginapessoal,
                  nif:nif,
                  apresentacao:apresentacao,
              activated: true,
              activated_at: Time.zone.now,
              actividadeprofissional: atividadeprofissional)

end


##Noticias com imagem e destaque = true
10.times do |n|
  titulo  = Faker::Lorem.word
  corpo = Faker::Lorem.sentence
  sumario="asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd "
  data=Faker::Date.backward(14)
  art=Article.create!(title:  titulo,
               content: corpo,
               sumario: sumario,
               data: data)
  art.picture=File.open(Rails.root.to_s + "/db/images/n.jpeg")
  art.save!
end
#noticias destaque =false sem imagem 
10.times do |n|
  titulo  = Faker::Lorem.word
  corpo = Faker::Lorem.sentence
  sumario="asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd "
  data=Faker::Date.backward(14)
  art=Article.create!(title:  titulo,
               content: corpo,
               sumario: sumario,
               data: data,
               destaque: false)

end
#noticias activated =false
10.times do |n|
  titulo  = Faker::Lorem.word
  corpo = Faker::Lorem.sentence
  sumario="asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd
  asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd "
  data=Faker::Date.backward(14)
  art=Article.create!(title:  titulo,
               content: corpo,
               sumario: sumario,
               data: data,
               activated: false)
  art.picture=File.open(Rails.root.to_s + "/db/images/n.jpeg")
  art.save!
end

##Cria ofertas

users = User.where(tipouser: 'entidade').take(6)
10.times do
  titulo  = Faker::Lorem.word
  content = Faker::Lorem.sentence(5)
  dataInicio = Faker::Date.forward(0)
  datafim = Faker::Date.forward(14)
  actividadeProf='Progamador Web'
  tipocontrato='Full Time'
  ativado=true
  salario="1000-1500"
  users.each { |user| user.offers.create!(content: content,
                                            title: titulo,
                                            dataInicio: dataInicio,
                                            dataFim: datafim,
                                            actividadeProf: actividadeProf,
                                            tipocontrato: tipocontrato,
                                            ativado:ativado,
                                            salario:salario) }
end

#cria relaçoes de seguidores
users = User.all
user  = users[1]
following = users[32..50]
followers = users[33..60]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }


#candidaturas User com id 2 , candidata-se a todas as ofertas 
ofertas =Offer.all
user=User.find(2)
ofertas.each{|oferta| user.candidatar(oferta)}
class DatabaseSchema < ActiveRecord::Migration


def change

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id"

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "tipouser",                              default: "candidato"
    t.string   "activation_digest"
    t.boolean  "activated",                             default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "morada"
    t.string   "localidade"
    t.string   "codPostal"
    t.string   "telemovel"
    t.datetime "datadenasc"
    t.string   "paginapessoal"
    t.string   "bi"
    t.string   "areaprof"
    t.string   "apresentacao"
    t.string   "niveldehab"
    t.string   "habilitlit"
    t.string   "situacaoprof"
    t.string   "cv"
    t.string   "picture"
    t.string   "expProf"
    t.string   "fax"
    t.string   "nif"
    t.string   "actividadeprofissional" ,    default: ""
    t.string   "paginaweb"
    t.integer  "{:index=>true, :foreign_key=>true}_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

  end
end

class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
    t.text     "title"
    t.text     "content"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "activated",  default: true
    t.boolean  "destaque",   default: true
    t.string   "sumario"
    t.string   "picture"
      t.timestamps null: false
    end
  end
end

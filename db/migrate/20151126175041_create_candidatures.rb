class CreateCandidatures < ActiveRecord::Migration
  def change
    create_table :candidatures do |t|
      t.integer :user_id, index: true
      t.integer :offer_id, index: true
      t.timestamps null: false
    end
add_index "candidatures", ["user_id", "offer_id"], name: "index_candidatures_on_user_id_and_offer_id", unique: true

  end
end

class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "title"
    t.text     "content"
    t.integer  "user_id"
    t.date     "dataInicio"
    t.date     "dataFim"
    t.text     "actividadeProf"
    t.text     "tipocontrato"
    t.text     "salario"
    t.boolean  "ativado",        default: false
    t.string   "picture"
      t.timestamps null: false
    end
        add_index "offers", ["user_id", "created_at"], name: "index_offers_on_user_id_and_created_at"
  add_index "offers", ["user_id"], name: "index_offers_on_user_id"
  end

end
